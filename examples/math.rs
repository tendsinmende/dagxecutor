/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use dagxecutor::*;
use std::{sync::{Arc, RwLock}, marker::PhantomData};

///Some more complex example. This time 3 types of node are implemented:
///
/// - Addition node over every type T that implements `Add` aka. addition.
/// - Receiver node, that just prints whatever it gets.
/// - SplitNode node, that takes some input, copies it once and sends the original and the copy
///   somewhere.
///
/// In addition the add node now has a name and implements the optional functions `debug_info()`
/// (used to supply debug information of this node to the aggregator) as well as `on_change` which gets
/// executed every time some input on the node changes.
struct Add<T>{
    name: String,
    t: std::marker::PhantomData<T>,
}

impl<T> Add<T>{
    fn new(name: &str) -> Self{
	Add{
	    name: name.to_owned(),
	    t: std::marker::PhantomData
	}
    }
}

///As you can see addition takes two dates, and adds them together. To make the execution a little time consuming,
/// we bussy wait for 4sec.
impl<T> Node for Add<T> where T: core::ops::Add<Output = T>, T: Send + 'static{
    type Inputs = (T, T);
    type Outputs = [T; 1];

    fn process(&self, input: Self::Inputs) -> Self::Outputs{
	println!("start");
	let start = std::time::Instant::now();

	let mut alter = false;
	while start.elapsed() < std::time::Duration::from_secs(4){
	    if alter{
		alter = false;
	    }else{
		alter = true;
	    }
	}
	
	
	[input.0 + input.1]
    }

    fn debug_info(&self) -> &str {
	 &self.name
    }

    fn on_change(&mut self, _aggregator: &Aggregator<Self::Inputs, Self::Outputs>) {
	println!("Addition Node:{} got some input", self.name);
    }
}

///The Receiver node just prints some value, similar to the HelloGraph node. The only
/// difference is, that it takes any data that is printable (`Debug` trait).
struct Receiver<T>{
    t: PhantomData<T>,
    start: std::time::Instant,
    flag: Arc<RwLock<bool>>,
}

impl<T> Receiver<T>{
    pub fn new(flag: Arc<RwLock<bool>>) -> Self{
	Receiver{
	    t: PhantomData,
	    start: std::time::Instant::now(),
	    flag
	}
    }
}

impl<T> Node for Receiver<T> where T: std::fmt::Debug + Send + 'static{
    type Inputs = [T;1];
    type Outputs = ();

    fn process(&self, input: Self::Inputs) -> Self::Outputs {
	println!("Got: {:?} after {}sec in graph", input[0], self.start.elapsed().as_secs_f32());
	*self.flag.write().unwrap() = true;
    }

    fn debug_info(&self) -> &str {
	"Generic-Print-Receiver"
    }
}

///As mentioned, the only thing this node does is to split some date.
///However similar to the add node this is abstracted over each type that can be cloned. So it does not care
/// if you are sending a f32 or some big struct as long as they implement Clone (and Send).
struct SplitNode<T>{t: PhantomData<T>}
impl<T> SplitNode<T> where T: Clone{
    pub fn new() -> Self{
	SplitNode{t: PhantomData}
    }
}

impl<T> Node for SplitNode<T> where T: Clone + Send + 'static{
    type Inputs = [T;1];
    type Outputs = (T,T);

    fn process(&self, input: Self::Inputs) -> Self::Outputs {
	println!("Split!");
	(input[0].clone(), input[0].clone())
    }

    fn debug_info(&self) -> &str {
	"Splitter-Node"
    }
}

///Similar to the hello graph example, a thread pool is setup. The graph setup follows.
/// finally in the async graph data is supplied to the "first" nodes in the graph.
/// The graphs looks a little like this:
///     _______      ______
/// 2---|split|------|add2|\   _______   __________
///     |_____|------|____| \--| add1|---|receiver|
///                  ______    |     |   |________|
/// 4----------------|add3|----|_____|
/// 5----------------|____|
fn main(){

    let ex = Executor::new_with_threads(2);
    let flag = Arc::new(RwLock::new(false));
    //Setup nodes
    let inner_node: Add<f32> = Add::new("singular 1");
    let add1_node = Aggregator::from_node(inner_node);

    let inner_node: Add<f32> = Add::new("parallel 2");
    let add2_node = Aggregator::from_node(inner_node);

    let inner_node: Add<f32> = Add::new("parallel 3");
    let add3_node = Aggregator::from_node(inner_node);

    let sn: SplitNode<f32> = SplitNode::new();
    let split_node = Aggregator::from_node(sn);

    
    let reciever_node: Receiver<f32> = Receiver::new(flag.clone());
    let output_node = Aggregator::from_node(reciever_node);

    //Setup graph from back to front.
    
    //Set edge from out of add to receiver node
    add1_node.write().unwrap().set_out_edge(Edge{out_idx: 0, in_idx: 0, target: output_node}).unwrap();

    //Edges from pre adds to merge add
    add2_node.write().unwrap().set_out_edge(Edge{out_idx: 0, in_idx: 0, target: add1_node.clone()}).unwrap();
    add3_node.write().unwrap().set_out_edge(Edge{out_idx: 0, in_idx: 1, target: add1_node.clone()}).unwrap();

    //Split to both inputs of node 2
    split_node.write().unwrap().set_out_edge(Edge{out_idx: 0, in_idx: 0, target: add2_node.clone()}).unwrap();
    split_node.write().unwrap().set_out_edge(Edge{out_idx: 1, in_idx: 1, target: add2_node.clone()}).unwrap();



    let exc = ex.clone();
    ex.schedule(Task::new(move ||split_node.write().unwrap().set_in(exc, 0 as usize, Box::new(2.0 as f32)).unwrap())).unwrap();

    let exc = ex.clone();
    let n3 = add3_node.clone();
    ex.schedule(Task::new(move ||n3.write().unwrap().set_in(exc, 0 as usize, Box::new(4.0 as f32)).unwrap())).unwrap();

    let exc = ex.clone();
    ex.schedule(Task::new(move ||add3_node.write().unwrap().set_in(exc, 1 as usize, Box::new(5.0 as f32)).unwrap())).unwrap();

    while !*flag.read().unwrap(){
	std::thread::sleep(std::time::Duration::from_millis(100));
    }
}
