/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use core::marker::PhantomData;
use dagxecutor::*;
use std::sync::{Arc, RwLock};
pub struct Add<T>{
    ty: PhantomData<T>,
    name: String,
}
impl<T> Add<T>{
    pub fn new(name: &str) -> Add<T>{
	Add{
	    ty: PhantomData,
	    name: name.to_owned(),
	}
    }
}
impl<T> Node for Add<T> where T: core::ops::Add<Output = T>, T: Send + 'static{
    type Inputs = (T, T);
    type Outputs = [T; 1];

    fn process(&self, input: Self::Inputs) -> Self::Outputs{
	[input.0 + input.1]
    }

    fn debug_info(&self) -> &str {
	&self.name
    }
}

pub struct Mul<T>{
    ty: PhantomData<T>,
    name: String,
}
impl<T> Mul<T>{
    pub fn new(name: &str) -> Mul<T>{
	Mul{
	    ty: PhantomData,
	    name: name.to_owned(),
	}
    }
}
impl<T> Node for Mul<T> where T: core::ops::Mul<Output = T>, T: Send + 'static{
    type Inputs = (T, T);
    type Outputs = [T; 1];

    fn process(&self, input: Self::Inputs) -> Self::Outputs{
	[input.0 * input.1]
    }

    fn debug_info(&self) -> &str {
	&self.name
    }
}

///Duplicates any incoming data into two
pub struct Dup<T>{
    ty: PhantomData<T>,
    name: String,
}
impl<T> Dup<T>{
    pub fn new(name: &str) -> Dup<T>{
	Dup{
	    ty: PhantomData,
	    name: name.to_owned(),
	}
    }
}
impl<T> Node for Dup<T> where T: Send + Clone + 'static{
    type Inputs = [T; 1];
    type Outputs = (T, T);

    fn process(&self, input: Self::Inputs) -> Self::Outputs{
	(input[0].clone(), input[0].clone())
    }

    fn debug_info(&self) -> &str {
	&self.name
    }
}

///The Receiver node just prints some value.
pub struct Receiver<T>{
    t: PhantomData<T>,
    name: String,
    flag: Arc<RwLock<bool>>,
}
impl<T> Receiver<T>{
    pub fn new(name: &str, flag: Arc<RwLock<bool>>) -> Self{
	Receiver{
	    t: PhantomData,
	    name: name.to_owned(),
	    flag
	}
    }
}
impl<T> Node for Receiver<T> where T: std::fmt::Debug + Send + 'static{
    type Inputs = [T;1];
    type Outputs = ();

    fn process(&self, input: Self::Inputs) -> Self::Outputs {
	println!("Receiver: {}: {:?}", self.name, input[0]);
	*self.flag.write().unwrap() = true;
    }

    fn debug_info(&self) -> &str {
	&self.name
    }
}

fn main(){
    let ex = Executor::new();
    //Create the graph object
    let graph = Arc::new(RwLock::new(Graph::new(1337)));
    let stop_f1 = Arc::new(RwLock::new(false));
    let stop_f2 = Arc::new(RwLock::new(false));
    
    //Add some nodes.
    let add_1 = graph.write().unwrap().add_node(Add::new("add 1") as Add<f32>);
    let add_2 = graph.write().unwrap().add_node(Add::new("add 2") as Add<f32>);
    let mul_1 = graph.write().unwrap().add_node(Mul::new("mul 1") as Mul<f32>);
    let mul_2 = graph.write().unwrap().add_node(Mul::new("mul 2") as Mul<f32>);
    let dup_1 = graph.write().unwrap().add_node(Dup::new("duplicate 1") as Dup<f32>);
    let dup_2 = graph.write().unwrap().add_node(Dup::new("duplicate 2") as Dup<f32>);

    //receiver nodes that are "outside" of this graph.
    let reciver_1 = Aggregator::from_node(Receiver::new("receiver 1", stop_f1.clone()) as Receiver<f32>);
    let reciver_2 = Aggregator::from_node(Receiver::new("receiver 2", stop_f2.clone()) as Receiver<f32>);
    
    //Connect nodes in a way that we get the graph
    // GRAPH(1337)                                  outside of 1337
    // __________________________________________
    // |     6               12         24      |
    //0|---|Add1|----------|Mul1|-----|Mul2|----|0---|receiver 1|
    //1|--/                /         /          |
    // |     2            /         /   2       |
    //2|---|Add2|--|dup1|/----|dup2|------------|1---|receiver 2|
    //3|--/                                     |
    // |________________________________________|
    //

    graph.write().unwrap().set_out_edge(Edge{ out_idx: 0, in_idx: 0, target: reciver_1.clone()}).unwrap();
    graph.write().unwrap().set_out_edge(Edge{ out_idx: 1, in_idx: 0, target: reciver_2.clone()}).unwrap();
    
    //Set in going connections
    graph.write().unwrap().connect_in_to_node(0, &add_1, 0).unwrap();
    graph.write().unwrap().connect_in_to_node(1, &add_1, 1).unwrap();

    graph.write().unwrap().connect_in_to_node(2, &add_2, 0).unwrap();
    graph.write().unwrap().connect_in_to_node(3, &add_2, 1).unwrap();
    //Set outgoing connections
    graph.write().unwrap().connect_node_to_out(&mul_2, 0, 0).unwrap();
    graph.write().unwrap().connect_node_to_out(&dup_2, 1, 1).unwrap();

    //Set in graph connections
    graph.read().unwrap().get_node(&add_1).unwrap().write().unwrap().set_out_edge(Edge{out_idx: 0, in_idx: 0, target: graph.read().unwrap().get_node(&mul_1).unwrap()}).unwrap();
    graph.read().unwrap().get_node(&mul_1).unwrap().write().unwrap().set_out_edge(Edge{out_idx: 0, in_idx: 0, target: graph.read().unwrap().get_node(&mul_2).unwrap()}).unwrap();

    graph.read().unwrap().get_node(&add_2).unwrap().write().unwrap().set_out_edge(Edge{out_idx: 0, in_idx: 0, target: graph.read().unwrap().get_node(&dup_1).unwrap()}).unwrap();
    graph.read().unwrap().get_node(&dup_1).unwrap().write().unwrap().set_out_edge(Edge{out_idx: 0, in_idx: 1, target: graph.read().unwrap().get_node(&mul_1).unwrap()}).unwrap();
    graph.read().unwrap().get_node(&dup_1).unwrap().write().unwrap().set_out_edge(Edge{out_idx: 1, in_idx: 0, target: graph.read().unwrap().get_node(&dup_2).unwrap()}).unwrap();
    graph.read().unwrap().get_node(&dup_2).unwrap().write().unwrap().set_out_edge(Edge{out_idx: 0, in_idx: 1, target: graph.read().unwrap().get_node(&mul_2).unwrap()}).unwrap();
    println!("Start graph with input");

    let exc = ex.clone();
    let gc = graph.clone();
    ex.schedule(Task::new(move ||gc.write().unwrap().set_in(exc, 0 as usize, Box::new(4.0 as f32)).unwrap())).unwrap();

    let exc = ex.clone();
    let gc = graph.clone();
    ex.schedule(Task::new(move ||gc.write().unwrap().set_in(exc, 1 as usize, Box::new(2.0 as f32)).unwrap())).unwrap();

    let exc = ex.clone();
    let gc = graph.clone();
    ex.schedule(Task::new(move ||gc.write().unwrap().set_in(exc, 2 as usize, Box::new(1.0 as f32)).unwrap())).unwrap();

    let exc = ex.clone();
    let gc = graph.clone();
    ex.schedule(Task::new(move ||gc.write().unwrap().set_in(exc, 3 as usize, Box::new(1.0 as f32)).unwrap())).unwrap();


    
    while !*stop_f1.read().unwrap() || !*stop_f2.read().unwrap(){
	std::thread::sleep(std::time::Duration::from_millis(100));
    }
}
