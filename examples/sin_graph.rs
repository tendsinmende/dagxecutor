/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use dagxecutor::*;
use std::sync::Mutex;

struct TickedProducer;

impl Node for TickedProducer{
    type Inputs = [bool;1];
    type Outputs = (Vec<f32>, bool);
    fn process(&self, input: Self::Inputs) -> Self::Outputs{
	println!("Process!");
	let buffer = vec![0.0; 1024];
	(buffer, true)
    }
    fn debug_info(&self) -> &str{
	"Prod"
    }
}

struct TickedVal;

impl Node for TickedVal{
    type Inputs = [bool;1];
    type Outputs = [f32; 1];
    fn process(&self, input: Self::Inputs) -> Self::Outputs{
	println!("Val!");
	[440.0]
    }
    fn debug_info(&self) -> &str{
	"Val"
    }
}

struct Sin;

impl Node for Sin{
    type Inputs = (Vec<f32>, f32);
    type Outputs = [Vec<f32>; 1];
    fn process(&self, input: Self::Inputs) -> Self::Outputs{
	println!("Sin!");
	let (mut buffer, freq) = input;
	let mut phase = 0.0;
	for s in &mut buffer{
	    phase += 2.0 * (core::f32::consts::PI * freq);
	    *s = phase.sin();
	}

	[buffer]
    }
    fn debug_info(&self) -> &str{
	"Sin"
    }
}

struct SendBack{
    s: Mutex<std::sync::mpsc::Sender<bool>>,
}

impl Node for SendBack{
    type Inputs = [Vec<f32>;1];
    type Outputs = ();
    fn process(&self, input: Self::Inputs) -> Self::Outputs{
	println!("Back");
	self.s.lock().unwrap().send(true).unwrap();
    }

    fn debug_info(&self) -> &str{
	"SendBack"
    }
}

fn main(){

    simple_logger::init().expect("Failed to init simple logger");

    let ex = Executor::new_with_threads(1);
    let (s, rec) = std::sync::mpsc::channel();
    
    //     __________________________                   _________    ___________
    //tick |Create Clock and buffer |-------------------|Sinus  |----|Send back|
    // ----|                        |                  /|_______|    |         |
    //     |                        |---|create val|--/              |_________|
    //     |                        |                           
    //     |________________________|


    let runs = 32;
    for n in 0..runs{


	let creator = Aggregator::from_node(TickedProducer);
	let val_create = Aggregator::from_node(TickedVal);
	let sin = Aggregator::from_node(Sin);
	let sb = Aggregator::from_node(SendBack{s: Mutex::new(s.clone())});

	creator.write().unwrap().set_out_edge(Edge{out_idx: 0, in_idx: 0, target: sin.clone()});
	creator.write().unwrap().set_out_edge(Edge{out_idx: 1, in_idx: 0, target: val_create.clone()});
	
	val_create.write().unwrap().set_out_edge(Edge{out_idx: 0, in_idx: 1, target: sin.clone()});

	sin.write().unwrap().set_out_edge(Edge{out_idx: 0, in_idx: 0, target: sb.clone()});

	
	let exc = ex.clone();
	let cc = creator.clone();
	ex.schedule(Task::new(move || cc.write().unwrap().set_in(exc, 0, Box::new(true)).unwrap())).unwrap();
    }

    let mut n = 0;
    while n < runs{
	if let Ok(gotten) = rec.recv(){
	    n += 1;
	    println!("Got: {}", n);
	}else{
	    std::thread::sleep(std::time::Duration::from_millis(10));
	}
    }
}
