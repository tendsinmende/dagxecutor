/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use dagxecutor::executor::{Executor, Task};
use log::info;
use simple_logger;
use rand::Rng;
use std::{time::{Instant, Duration}, sync::{RwLock, Arc}};


fn fibbo(i: usize) -> usize{
    if i == 0{
	0
    }else if i == 1{
	1
    }else{
	fibbo(i - 1) + fibbo(i - 2)
    }
}

fn random_calc(old: usize, times: usize, executor: Arc<Executor>, end_flag: Arc<RwLock<i32>>){
    info!("Calc time: {}", times);
    if times == 0{
	*end_flag.write().unwrap() += 1;
	info!("Finish");
	return;
    }

    let mut rng = rand::thread_rng();
    let iter_steps = rng.gen_range(25, 40);
    let fib = fibbo(iter_steps);
    let b = fib + 1;
    
    //Spawn new tasks
    let e1 = executor.clone();
    let e2 = executor.clone();

    let f1 = end_flag.clone();
    
    let t1 = Task::new(move ||random_calc(b, times - 1, e1, f1));
    let t2 = Task::new(move ||random_calc(b, times - 1, e2, end_flag));
    executor.schedule(t1).expect("Failed to execute thread!");
    executor.schedule(t2).expect("Failed to execute thread");
}


fn main(){
    let start = Instant::now();
    let num_ex = 11;
    let finished = Arc::new(RwLock::new(0 as i32));
    //simple_logger::init().unwrap();
    let ex = Executor::new();
    let e1 = ex.clone();
    let flag = finished.clone();
    let task = Task::new(move ||random_calc(0, num_ex, e1, flag));
    ex.schedule(task).expect("Failed to schedule main!");

    while *finished.read().unwrap() < (2 as i32).pow(num_ex as u32){
	std::thread::sleep(Duration::from_millis(100));
    }

    println!("Took {} sec", start.elapsed().as_secs_f32());
}
