/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use criterion::{black_box, criterion_group, criterion_main, Criterion};
use std::sync::{Arc, RwLock, Mutex};
use dagxecutor::*;

struct TickedProducer;

impl Node for TickedProducer{
    type Inputs = [bool;1];
    type Outputs = (Vec<f32>, bool);
    fn process(&self, input: Self::Inputs) -> Self::Outputs{
	let buffer = vec![0.0; 1024];
	(buffer, true)
    }
    fn debug_info(&self) -> &str{
	"Prod"
    }
}

struct TickedVal;

impl Node for TickedVal{
    type Inputs = [bool;1];
    type Outputs = [f32; 1];
    fn process(&self, input: Self::Inputs) -> Self::Outputs{
	[440.0]
    }
    fn debug_info(&self) -> &str{
	"Val"
    }
}

struct Sin;

impl Node for Sin{
    type Inputs = (Vec<f32>, f32);
    type Outputs = [Vec<f32>; 1];
    fn process(&self, input: Self::Inputs) -> Self::Outputs{
	let (mut buffer, freq) = input;
	let mut phase = 0.0;
	for s in &mut buffer{
	    phase += 2.0 * (core::f32::consts::PI * freq);
	    *s = phase.sin();
	}

	[buffer]
    }
    fn debug_info(&self) -> &str{
	"Sin"
    }
}

struct SendBack{
    s: Mutex<std::sync::mpsc::Sender<bool>>,
}

impl Node for SendBack{
    type Inputs = [Vec<f32>;1];
    type Outputs = ();
    fn process(&self, input: Self::Inputs) -> Self::Outputs{
	self.s.lock().unwrap().send(true).unwrap();
    }

    fn debug_info(&self) -> &str{
	"SendBack"
    }
}

//Graph used: 
//     __________________________                   _________    ___________
//tick |Create Clock and buffer |-------------------|Sinus  |----|Send back|
// ----|                        |                  /|_______|    |         |
//     |                        |---|create val|--/              |_________|
//     |                        |                           
//     |________________________|
fn run_graph(ex: Arc<Executor>){

    let (s, rec) = std::sync::mpsc::channel();
    
    let creator = Aggregator::from_node(TickedProducer);
    let val_create = Aggregator::from_node(TickedVal);
    let sin = Aggregator::from_node(Sin);
    let sb = Aggregator::from_node(SendBack{s: Mutex::new(s.clone())});

    creator.write().unwrap().set_out_edge(Edge{out_idx: 0, in_idx: 0, target: sin.clone()});
    creator.write().unwrap().set_out_edge(Edge{out_idx: 1, in_idx: 0, target: val_create.clone()});
    
    val_create.write().unwrap().set_out_edge(Edge{out_idx: 0, in_idx: 1, target: sin.clone()});

    sin.write().unwrap().set_out_edge(Edge{out_idx: 0, in_idx: 0, target: sb.clone()});

    
    let exc = ex.clone();
    let cc = creator.clone();
    ex.schedule(Task::new(move || cc.write().unwrap().set_in(exc, 0, Box::new(true)).unwrap())).unwrap();

    rec.recv().expect("Could not get!");
}

fn criterion_benchmark(c: &mut Criterion) {
    let ex = Executor::new(); //Setup executor
    
    c.bench_function("sync_bench", |b| {
	b.iter(|| run_graph(ex.clone()))
    });
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
