/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use crate::*;
use crate::log::error;
use std::sync::{Arc, RwLock};
use std::marker::PhantomData;
use core::any::{Any, TypeId};


impl InputCollector for () {
    type Collection = ();
    fn set(&mut self, _index: usize, _val: Box<dyn Any + Send>) -> Result<(), GraphError>{
	Err(GraphError::ExceedsIndex(0))
    }

    fn all_set(&self) -> bool {
	false
    }

    fn take(&mut self) -> Self::Collection{
	()
    }

    fn is_set(&self, index: usize) -> bool {
	false
    }
}
impl<A> InputCollector for Option<Box<A>> where A: Send + 'static{
    type Collection = [A;1];
    fn set(&mut self, index: usize, val: Box<dyn Any + Send>) -> Result<(), GraphError>{	
	//Try to parse th values to the n'th data
	match index{
	    0 => {
		if val.is::<A>(){
		    *self = Some(val.downcast::<A>().unwrap());
		    Ok(())
		}else{
		    error!("1 Value is: {:?} should be: {:?}", val.type_id(), TypeId::of::<A>());
		    Err(GraphError::WrongDataType(TypeId::of::<A>()))
		}
	    }
	    _ => Err(GraphError::ExceedsIndex(1))
	}
    }

    fn all_set(&self) -> bool {
	self.is_some()
    }

    fn take(&mut self) -> Self::Collection{
	debug_assert!(self.all_set(), "Not all futures where set!");
	[*self.take().unwrap()]
    }

    fn is_set(&self, index: usize) -> bool {
	if index == 0{
	    self.is_some()
	}else{
	    false
	}
    }
}


//----INTOS-----

impl IntoInputCollector for (){
    type Collector = ();
    fn to_collector() -> Self::Collector{
	()
    }
    fn get_type_id(_index: usize) -> Result<TypeId, GraphError>{
	Err(GraphError::ExceedsIndex(0))
    }
}

///Special implementation for a single data type `A`. Has to be this way since specialization has not yet landed in rust.
impl<A> IntoInputCollector for [A; 1] where A: Send + 'static{
    type Collector = Option<Box<A>>;
    fn to_collector() -> Self::Collector{
	None
    }
    fn get_type_id(index: usize) -> Result<TypeId, GraphError>{
	if index == 0{
	    Ok(TypeId::of::<A>())
	}else{
	    Err(GraphError::ExceedsIndex(1))
	}
    }
}


///Implemets the input_collector trait based on the same input as the impl_splitter macro.
#[macro_export]
macro_rules! impl_input_collector {
    ( $( $Gen:ident : $Idx:tt ),+, $Max:literal ) => {
        impl<$($Gen),+> InputCollector for ($(Option<Box<$Gen>>),+) where $($Gen : Send + 'static),+{
	    type Collection = ($($Gen),+);
	    fn set(&mut self, index: usize, val: Box<dyn Any + Send>) -> Result<(), GraphError>{
		//Try to parse th values to the n'th data
		match index{
		    $($Idx => {			
			if let Ok(cast) = val.downcast::<$Gen>(){
			    self.$Idx = Some(cast);
			    Ok(())
			}else{
			    error!("{} Could not cast to {}!", $Idx, std::any::type_name::<$Gen>());
			    Err(GraphError::WrongDataType(TypeId::of::<$Gen>()))
			}
		    }),+
		    
		    _ => Err(GraphError::ExceedsIndex($Max))
		}
	    }

	    fn all_set(&self) -> bool {
		$(self.$Idx.is_some() && )+ true
	    }

	    fn take(&mut self) -> Self::Collection{
		debug_assert!(self.all_set(), "Not all futures where set!");
		($(*self.$Idx.take().unwrap()),+)
	    }

	    fn is_set(&self, index: usize) -> bool{
		match index{
		    $($Idx => self.$Idx.is_some(), )+
		    _ => false
		}
	    }
	}
    };
}

impl_input_collector!(A:0, B:1, 1);
impl_input_collector!(A:0, B:1, C:2, 2);
impl_input_collector!(A:0, B:1, C:2, D:3, 3);
impl_input_collector!(A:0, B:1, C:2, D:3, E:4, 4);
impl_input_collector!(A:0, B:1, C:2, D:3, E:4, F:5, 5);
impl_input_collector!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, 6);
impl_input_collector!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, 7);
impl_input_collector!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, I:8, 8);
impl_input_collector!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, I:8, J:9, 9);
impl_input_collector!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, I:8, J:9, K:10, 10);
impl_input_collector!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, I:8, J:9, K:10, L:11, 11);
impl_input_collector!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, I:8, J:9, K:10, L:11, M:12, 12);
impl_input_collector!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, I:8, J:9, K:10, L:11, M:12, N:13, 13);
impl_input_collector!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, I:8, J:9, K:10, L:11, M:12, N:13, O:14, 14);
impl_input_collector!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, I:8, J:9, K:10, L:11, M:12, N:13, O:14, P:15, 15);
impl_input_collector!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, I:8, J:9, K:10, L:11, M:12, N:13, O:14, P:15, Q:16, 16);
impl_input_collector!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, I:8, J:9, K:10, L:11, M:12, N:13, O:14, P:15, Q:16, R:17, 17);
impl_input_collector!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, I:8, J:9, K:10, L:11, M:12, N:13, O:14, P:15, Q:16, R:17, S:18, 18);
impl_input_collector!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, I:8, J:9, K:10, L:11, M:12, N:13, O:14, P:15, Q:16, R:17, S:18, T:19, 19);
impl_input_collector!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, I:8, J:9, K:10, L:11, M:12, N:13, O:14, P:15, Q:16, R:17, S:18, T:19, U:20, 20);
impl_input_collector!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, I:8, J:9, K:10, L:11, M:12, N:13, O:14, P:15, Q:16, R:17, S:18, T:19, U:20, V:21, 21);
impl_input_collector!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, I:8, J:9, K:10, L:11, M:12, N:13, O:14, P:15, Q:16, R:17, S:18, T:19, U:20, V:21, W:22, 22);
impl_input_collector!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, I:8, J:9, K:10, L:11, M:12, N:13, O:14, P:15, Q:16, R:17, S:18, T:19, U:20, V:21, W:22, X:23, 23);
impl_input_collector!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, I:8, J:9, K:10, L:11, M:12, N:13, O:14, P:15, Q:16, R:17, S:18, T:19, U:20, V:21, W:22, X:23, Y:24, 24);
impl_input_collector!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, I:8, J:9, K:10, L:11, M:12, N:13, O:14, P:15, Q:16, R:17, S:18, T:19, U:20, V:21, W:22, X:23, Y:24, Z:25, 25);

///Implements the into_input_collector trait based on the same input as the impl_splitter macro.
#[macro_export]
macro_rules! impl_into_input_collector {
    ( $( $Gen:ident : $Idx:tt ),+, $Max:literal ) => {
        
	impl<$($Gen),+> IntoInputCollector for ($($Gen),+) where $($Gen : Send + 'static),+{
	    type Collector = ($(Option<Box<$Gen>>),+);

	    fn to_collector() -> Self::Collector{
		use core::option::Option;
		($(Option::<Box<$Gen>>::None),+)
	    }
	    
	    fn get_type_id(index: usize) -> Result<TypeId, GraphError>{
		match index{
		    $(
			$Idx => {Ok(TypeId::of::<$Gen>())}
		    ), +
		    _ => Err(GraphError::ExceedsIndex($Max)),
		}
	    }
	}
    };
}

impl_into_input_collector!(A:0, B:1, 1);
impl_into_input_collector!(A:0, B:1, C:2, 2);
impl_into_input_collector!(A:0, B:1, C:2, D:3, 3);
impl_into_input_collector!(A:0, B:1, C:2, D:3, E:4, 4);
impl_into_input_collector!(A:0, B:1, C:2, D:3, E:4, F:5, 5);
impl_into_input_collector!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, 6);
impl_into_input_collector!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, 7);
impl_into_input_collector!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, I:8, 8);
impl_into_input_collector!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, I:8, J:9, 9);
impl_into_input_collector!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, I:8, J:9, K:10, 10);
impl_into_input_collector!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, I:8, J:9, K:10, L:11, 11);
impl_into_input_collector!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, I:8, J:9, K:10, L:11, M:12, 12);
impl_into_input_collector!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, I:8, J:9, K:10, L:11, M:12, N:13, 13);
impl_into_input_collector!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, I:8, J:9, K:10, L:11, M:12, N:13, O:14, 14);
impl_into_input_collector!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, I:8, J:9, K:10, L:11, M:12, N:13, O:14, P:15, 15);
impl_into_input_collector!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, I:8, J:9, K:10, L:11, M:12, N:13, O:14, P:15, Q:16, 16);
impl_into_input_collector!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, I:8, J:9, K:10, L:11, M:12, N:13, O:14, P:15, Q:16, R:17, 17);
impl_into_input_collector!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, I:8, J:9, K:10, L:11, M:12, N:13, O:14, P:15, Q:16, R:17, S:18, 18);
impl_into_input_collector!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, I:8, J:9, K:10, L:11, M:12, N:13, O:14, P:15, Q:16, R:17, S:18, T:19, 19);
impl_into_input_collector!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, I:8, J:9, K:10, L:11, M:12, N:13, O:14, P:15, Q:16, R:17, S:18, T:19, U:20, 20);
impl_into_input_collector!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, I:8, J:9, K:10, L:11, M:12, N:13, O:14, P:15, Q:16, R:17, S:18, T:19, U:20, V:21, 21);
impl_into_input_collector!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, I:8, J:9, K:10, L:11, M:12, N:13, O:14, P:15, Q:16, R:17, S:18, T:19, U:20, V:21, W:22, 22);
impl_into_input_collector!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, I:8, J:9, K:10, L:11, M:12, N:13, O:14, P:15, Q:16, R:17, S:18, T:19, U:20, V:21, W:22, X:23, 23);
impl_into_input_collector!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, I:8, J:9, K:10, L:11, M:12, N:13, O:14, P:15, Q:16, R:17, S:18, T:19, U:20, V:21, W:22, X:23, Y:24, 24);
impl_into_input_collector!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, I:8, J:9, K:10, L:11, M:12, N:13, O:14, P:15, Q:16, R:17, S:18, T:19, U:20, V:21, W:22, X:23, Y:24, Z:25, 25);
