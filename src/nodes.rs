/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use log::{info, warn};
use std::sync::{Arc, RwLock};
use std::marker::PhantomData;
use core::any::{Any, TypeId};
use crate::{Executor, producer::ProducerAble, Task};

///All possible errors that can occure.
#[derive(Debug, PartialEq, Eq)]
pub enum GraphError{
    ///Type Id was wrong, should be the associated one.
    WrongDataType(TypeId),
    ///The given index exceeds the nodes input range, max index is the associated one.
    ExceedsIndex(usize),
    ///If this node has no output, then no out edge can be set.
    NoOutput,
    ///Happens if an edge is added where to data type at one node does not match the data type at the second one.
    DataTypeMissmatch{
	type_at_from: TypeId,
	type_at_to: TypeId
    },
    ///While connecting an edge if the used out_index is already in use
    OutIndexInUse(usize),
    ///If some edge should be removed, but is not actually in use.
    OutIndexNotInUse(usize), 
    ///While connecting an edge if the used in_index is already in use.
    InIndexInUse(usize),
    ///If the node was not found in the graph
    NodeNotFound,
    ///If this node has no out ports
    NoOutPorts,
    ///If the out index of a bridge is needed but not set. For instance if the type id for an bridge index is asked for, but no outgoing
    /// edge for this index was set before hand.
    BridgeIndexNotSet(usize, String)
}

impl std::fmt::Display for GraphError{
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
	match self{
	    GraphError::WrongDataType(t) => write!(f, "WrongDataType(typeId={:?})", t),
	    GraphError::ExceedsIndex(i) => write!(f, "Exceedsindex(max_index={})", i),
	    GraphError::NoOutput => write!(f, "NoOutput(usually happens if you try to set an output edge for an node without outputs)"),
	    GraphError::DataTypeMissmatch{type_at_from, type_at_to} => write!(f, "DatatypeMissmatch(\n    type at producer node: {:?}\n    type at consumer node: {:?})", type_at_from, type_at_to),
	    GraphError::OutIndexInUse(i) => write!(f, "OutIndexInUse(While setting an edge tried to overwrite the connection of another node on the same out_index of this producer.) out_index={}", i),
	    GraphError::OutIndexNotInUse(i) => write!(f, "OutIndexNotInUse(Some edge should be removed, but no edge is registered at this out_index={})", i),
	    GraphError::InIndexInUse(i) => write!(f, "InIndexInUse(Tried to set an in_index for a consumer node while adding an edge to a producer. But this in_index is already in use by some other node) in_index={}", i),
	    GraphError::NodeNotFound => write!(f, "NodeNotFound(Node (usually based on some NodeId wasn't found. Maybe its not (anymore?) in this graph.))"),
	    GraphError::NoOutPorts => write!(f, "NoOutPorts(The node on which split was called has no output ports)"),
	    GraphError::BridgeIndexNotSet(i, n) => write!(f, "BridgeIndexNotSet(index: {}, name: {}) some information of a bridge index was needed, but this index has not yet received a outgoing edge.", i, n)
	}
        
    }
}

///An edge that can be added to some node as outgoing edge via `AbstractAggregator.set_out_edge(edge)`.
pub struct Edge{
    ///The data index that will be send from the producer to the consumer.
    ///
    /// For example if you have a node producing a f32 and you want it to be send to some nodes 2-nd input, this
    ///would be 0, since the output index would be 0.
    pub out_idx: usize,
    ///The index of the consumers input at which the edge will be docked. Based on the example of `out_index` this would be
    /// 1, since we want to send the data to the second input aka. the input at index 1.
    pub in_idx: usize,
    ///The consumer/target of the produces data that comes out of `out_index`. Again: the producer is the aggregator this
    /// edge is added to.
    pub target: Arc<RwLock<dyn AbstAggregator + Send + Sync>>,
}

///Abstract trait that must be implemented for all input types. Is default implemented for all tuple with less then
/// 11 Memebers, which should be enough inputs for most nodes. However, the `impl_input_collector_tuple` macro can be used
/// to impl it for more.
///
/// # Special case A
///
/// For single data of some type this must be `[A;1]` instead of `A` since trait specialization has not yet landed in
/// rust. have a look at the impl for more details.
pub trait InputCollector{
    type Collection: IntoInputCollector;
    fn set(&mut self, index: usize, val: Box<dyn Any + Send>) -> Result<(), GraphError>;
    fn all_set(&self) -> bool;
    ///Unpacks all staged futures. Executes them on a task and waits for the result. Assumes that `all_set()` would return true. 
    fn take(&mut self) -> Self::Collection;
    fn is_set(&self, index: usize) -> bool;
}

///Trait that transforms any implementing type to its collector type.
///
/// # limitations
/// There is no implementation for the empty type `()`. So nodes with out input are not supported. The reason for that is, that a node gets executed when the last input is ready. This cannot happen without inputs. Therefore each node must have at least one input.
///
/// # overcoming this limitation
/// So what do I do if I have some kind of "producer" node? The easiest way is to make an boolean input, that gets set from your graph-executer, every time the graph needs to be calculated. This bool gets set and the node can fire, without actually using the input.
pub trait IntoInputCollector where Self: Sized{
    type Collector: InputCollector<Collection = Self> + Send;
    fn to_collector() -> Self::Collector;
    fn get_type_id(index: usize) -> Result<TypeId, GraphError>;
}

///The actual splitter which keeps track of the internal data type and where to send data.
pub struct SplitRec<T>{
    pub receiver: Option<Arc<RwLock<dyn AbstAggregator + Send + Sync>>>,
    pub target_index: usize,
    pub type_id: TypeId,
    pub phantom: PhantomData<T>
}


///The associated output of the `IntoSplitter` trait. Is default implemented for the same types as `IntoSplitter`.
pub trait Splitter{
    type DataLayout;
    fn set_receiver(&mut self, edge: Edge) -> Result<(), GraphError>;
    fn remove_edge(&mut self, index: usize) -> Result<(), GraphError>;
    fn split(&self, executor: Arc<Executor>, data: Self::DataLayout, node_info: &str) -> Result<(), GraphError>;
    fn is_set(&self, index: usize) -> bool;
}

///Must be implemented for a type to be used as `Outputs` within a `Node`.
///
///Is default implemented for `()`, `[A;1]` (the implementation for a single value of type A), and every
///tuple of the pattern `(A, B)` to `(A, B, C, ..., Z)`. So when implementing any node with up to 26 outputs, there is no need
///to implement the trait yourself. Nodes with more outputs should probably be abstracted into a sub-graph until outputs are collapsed.
///Or the output is collapsed into some data type (for instance a struct holding the > 26 dates).
pub trait IntoSplitter where Self: Sized{
    type SplitterType: Splitter<DataLayout = Self> + Send;
    fn to_splitter() -> Self::SplitterType;
    fn get_type_id(index: usize) -> Result<TypeId, GraphError>;
}

///Collect the input futures of the inner node, handles node execution and transport of outputs to the correct
/// follow up aggregator.
pub struct Aggregator<I,O> where I: IntoInputCollector, O: IntoSplitter{
    node: Arc<RwLock<dyn Node<Inputs=I, Outputs=O> + Send + Sync>>,
    inputs: Arc<RwLock<dyn InputCollector<Collection = I> + Send + Sync>>,
    outputs: Arc<RwLock<dyn Splitter<DataLayout = O> + Send + Sync>>,
}

impl<I, O> Aggregator<I, O>
where I: IntoInputCollector + Send + 'static,
      O: IntoSplitter + Send + 'static,
<O as IntoSplitter>::SplitterType: Sync,
<I as IntoInputCollector>::Collector: Sync
{
    ///Takes some node value and creates and aggregator from it. Should be used, if the content of the node is not used on any
    /// other context then this graph. If you need access to the node after adding, use `from_arc` (or `from_arc_producer`).
    pub fn from_node<N>(node: N) -> Arc<RwLock<Self>> where N: Node<Inputs = I, Outputs = O> + Sync + Send + 'static{
	Arc::new(RwLock::new(Aggregator{
	    node: Arc::new(RwLock::new(node)),
	    inputs: Arc::new(RwLock::new(I::to_collector())),
	    outputs: Arc::new(RwLock::new(O::to_splitter())),
	}))
    }

    ///Similar to the `from_node` function. However, this node is already wrapped in an `Arc<RwLock<N>>`. So you can clone it before adding
    /// it to the graph. That's why you can access the node even after adding it to the graph.
    ///
    /// # Why `Arc<RwLock<N>>`?
    /// Since the graph get automatically multi threaded, the node needs to be shareable across threads. Currently RwLock is used, since
    /// i think, reading will occur more often than writing. However, I did not benchmark it as of version 0.0.1, so this might change to a Mutex.
    pub fn from_arc<N>(node: Arc<RwLock<N>>) -> Arc<RwLock<Self>> where N: Node<Inputs = I, Outputs = O> + Sync + Send + 'static{
	Arc::new(RwLock::new(Aggregator{
	    node,
	    inputs: Arc::new(RwLock::new(I::to_collector())),
	    outputs: Arc::new(RwLock::new(O::to_splitter())),
	}))
    }

    ///Takes a node that implements the producer trait. The creation process will give the splitter to the node.
    /// This splitter can for instance be used in other threads to send data from this node.
    pub fn from_arc_producer<N>(node: Arc<RwLock<N>>) -> Arc<RwLock<Self>>
    where N: Node<Inputs = I, Outputs = O> + ProducerAble<OutputLayout = O> + Send + Sync + 'static{
	let splitter = Arc::new(RwLock::new(O::to_splitter()));
	//Call
	node.write().unwrap().receiver_channel(splitter.clone());

	Arc::new(RwLock::new(Aggregator{
	    node,
	    inputs: Arc::new(RwLock::new(I::to_collector())),
	    outputs: splitter
	}))
    }
    
    fn execute(&mut self, executor: Arc<Executor>){
	let inputs = self.inputs.write().unwrap().take();
	let cloned_node = self.node.clone();
	let cloned_splitter = self.outputs.clone();
	let debug_info = String::from(self.node.read().unwrap().debug_info());
	let ex_clone = executor.clone();
	
	executor.schedule(Task::new(move||{
	    let output = cloned_node.write().unwrap().process(inputs);
	    if let Err(e) = cloned_splitter.write().unwrap().split(ex_clone, output, cloned_node.read().unwrap().debug_info()){
		if e != GraphError::NoOutPorts{
		    	warn!("AsyncGraph: WARNING: failed to execute splitter on {}: {}", debug_info, e);
		}
	    }
	})).unwrap();
    }
}

pub trait AbstAggregator{
    ///Sets the input at `index` to `val`. Returns an error if either the index exceeds the inputs of the aggregator, or the value at `index` as another data type.
    fn set_in(&mut self, executor: Arc<Executor>, index: usize, val: Box<dyn Any + Send>) -> Result<(), GraphError>;
    ///Sets The receiver for the data that gets returned. Returns an error:
    ///
    /// 1. If the data type at
    /// `from_index` (data type of `self` at `from_index`) does not match the data type at the `to` aggregator
    /// at `to_index`
    ///
    /// 2. The `to_index` it too big for the `to` aggregator
    ///
    /// 3. The `from_index` is to big for `self` aggregator
    fn set_out_edge(&mut self, edge: Edge) -> Result<(), GraphError>;

    ///Removes the edge at `index`. Returns an error if either no edge is connected at this index, of the index exceeds the outputs
    /// of this node.
    fn remove_out_edge(&mut self, index: usize) -> Result<(), GraphError>;
    
    ///Provides the type id of the input argument at `index` for the inner `node`
    fn get_in_type_id(&self, index: usize) -> Result<TypeId, GraphError>;

    ///Provides the type id of the inner nodes output at `index`
    fn get_out_type_id(&self, index: usize) -> Result<TypeId, GraphError>;

    ///Returns true if this input idx is set
    fn is_in_set(&self, index: usize) -> bool;

    ///Returns true if this output idx is set
    fn is_out_set(&self, index: usize) -> bool;
}

impl<I, O> AbstAggregator for Aggregator<I, O>
where I: IntoInputCollector + Send + 'static,
      O: IntoSplitter + Send + 'static,
      <O as IntoSplitter>::SplitterType: Sync,
      <I as IntoInputCollector>::Collector: Sync{
    fn set_in(&mut self, executor: Arc<Executor>, index: usize, val: Box<dyn Any + Send>) -> Result<(), GraphError> {
	self.inputs.write().unwrap().set(index, val)?;

	//Notify node about some change
	self.node.write().unwrap().on_change(&self);
	
	//When all inputs are set, execute the futures we gathered and schedule our own work
	if self.inputs.read().unwrap().all_set(){
	    info!("Node [{}] got all inputs, is executing!", self.node.read().unwrap().debug_info());
	    self.execute(executor);
	}else{
	    //info!("Node [{}] as not yet all inputs, waiting.", self.node.read().unwrap().debug_info());
	}
	
	Ok(())
    }

    fn remove_out_edge(&mut self, index: usize) -> Result<(), GraphError> {
	self.outputs.write().unwrap().remove_edge(index)
    }

    fn set_out_edge(&mut self, edge: Edge) -> Result<(), GraphError>{
	info!(
	    "setting edge of [{}] with Edge[source_idx: {}, target_in: {}]",
	    self.node.read().unwrap().debug_info(),
	    edge.out_idx,
	    edge.in_idx
	);
	self.outputs.write().unwrap().set_receiver(edge)
    }
    
    fn get_in_type_id(&self, index: usize) -> Result<TypeId, GraphError>{
	I::get_type_id(index)
    }

    fn get_out_type_id(&self, index: usize) -> Result<TypeId, GraphError>{
	O::get_type_id(index)
    }

    fn is_in_set(&self, index: usize) -> bool{
	self.inputs.read().unwrap().is_set(index)
    }
    
    fn is_out_set(&self, index: usize) -> bool{
	self.outputs.read().unwrap().is_set(index)
    }
}

///The center trait of this crate.
///
///Definition for some node that can be executed.
///The only mandatory function is `process()` which takes some `input` and produces some output. The types of both
///are the associated types `Inputs` and `Outputs` which can freely be chosen, as long as they implement
///`IntoInputcollector` and `IntoSplitter` respectively. Both traits can be implemented by hand, however the easiest way is
///to use tuples of types that implement `Send`, since for those the traits are already implemented.
///
///# Example: A Addition node
///
///This shows and easy way to implement addition as node on every type `T` that implements `Add`.
///Observe that the relation *(RxR) -> R* is represented by `Inputs = (T,T); Outputs = [T;1]`. Where the `[T;1]` is some
///trick to represent single data types I hope to eliminated later.
///```
///struct Add<T>{
///    t: std::marker::PhantomData<T>,
///}
///
///impl<T> Add<T>{
///    fn new() -> Self{
///        Add{
///            t: std::marker::PhantomData
///        }
///    }
///}
///
///impl<T> Node for Add<T> where T: core::ops::Add<Output = T>, T: Send + 'static{
///    type Inputs = (T, T);
///    type Outputs = [T; 1];
///
///    fn process(&self, input: Self::Inputs) -> Self::Outputs{
///        [input.0 + input.1]
///    }
///}
///```
pub trait Node{
    type Inputs: IntoInputCollector;
    type Outputs: IntoSplitter;

    ///Mandatory process function that declares the behavior of this node
    fn process(&self, input: Self::Inputs) -> Self::Outputs;

    ///Gets called by the aggregator every time some input-state changes
    fn on_change(&mut self, _aggregator: &Aggregator<Self::Inputs, Self::Outputs>){
	
    }

    ///String passed to errors that occure in the graph. Can be used to easily identify the node in the graph.
    fn debug_info(&self) -> &str{
	""
    }
}
