/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use crate::*;
use std::sync::{Arc, RwLock};
use std::marker::PhantomData;
use core::any::{Any, TypeId};


///Must be implemented by any node that wants to be a producer. The `receive_channel` functions gets called when the node is added
/// to some graph. In that case the node receives the Splitter, that sends the `Outputs` the node produces.
///
/// This channel can, for instance be saved, and, in another thread be used to send data every `n` seconds.
pub trait ProducerAble{
    type OutputLayout: IntoSplitter;
    fn receiver_channel(&mut self, channel: Arc<RwLock<dyn Splitter<DataLayout = Self::OutputLayout> + Send + Sync>>);
    fn produce(&self);
}
