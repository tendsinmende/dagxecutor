/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

extern crate log;
extern crate crossbeam_channel as cc;
extern crate num_cpus;

///the all mighty executor on which all nodes will push their jobs
pub mod executor;
pub use executor::{Executor, Task};

///Main node implementation as well as definition of the aggregator;
pub mod nodes;
pub use nodes::*;

///Input related implementations
pub mod inputs;
pub use inputs::*;
///Splitter related implementation
pub mod splitter;
pub use splitter::*;
///Graph abstraction related implementation.
pub mod graph;
pub use graph::*;

///A bridge works like a node that only routes inputs.
pub mod bridge;
pub use bridge::*;

///Provides the producer trait that can be implemented for nodes, that produce
/// an asynchronous signal.
pub mod producer;
pub use producer::*;
